#!/bin/bash
curl https://s3-ap-southeast-1.amazonaws.com/bitzotik.in/data.txt > course.txt
value=$(<course.txt)
chapters=()
while read line; do
	chapters+=($line)
done <<< "$value"

moveSteps(){
	echo "$1"
	IFS='=' read -ra parts <<< "${chapters[$1]}"
	git checkout "${parts[0]}"
	IFS=',' read -ra steps <<< "${parts[1]}"
	pointer=0
	git reset --hard "${steps[$pointer]}"
	stepCount=${#steps[@]}
	echo "$stepCount"
	read command
	while [ $command != "back" ]; do
	if [ $command = "next" ];
	then
		echo "moving next"
		((pointer++))
		if [ "$pointer" = "$stepCount" ]; then
			return
		fi
		git reset --hard "${steps[$pointer]}"
		sh ./mybuild.sh
	fi
	read command
	done
}

showChapters(){
	command=""
	while [ "$command" != "quit" ]; do
	i=1
	for chapter in ${chapters[@]}
	do
	IFS='=' read -ra parts <<< "$chapter"
	echo "$i. ${parts[0]}"
	((i++))
	done
	read command
	if [ $command != "quit" ]; then
		((command--))
		moveSteps $command
	fi
	done
}

showChapters
